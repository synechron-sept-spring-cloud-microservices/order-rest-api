# Order Microservice

## Fetch the configuration from config server

## Dependencies

```xml
	<properties>
		<java.version>1.8</java.version>
		<spring-cloud.version>2020.0.3</spring-cloud.version>
	</properties>
    <depedencies>
   		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-config</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-bootstrap</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
		</dependency>

    </dependencies>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>
```
## Bootstrap properties
```yml
spring:
  application:
    name: orderservice
  profiles:
    active: dev
  cloud:
    config:
      uri: http://localhost:8888

```

## Application configuration
```yml
server:
  port: 8222
```

Add the `@EnableDiscoveryClient` annotation in the root class

## Testing
1. Start the server and test if the server starts listening on `8222` port.
2. Verify in the Eureka server, if the instance is getting registered.

