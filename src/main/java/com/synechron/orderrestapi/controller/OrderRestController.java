package com.synechron.orderrestapi.controller;

import com.synechron.orderrestapi.model.Order;
import com.synechron.orderrestapi.service.OrderService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderRestController {

    private final OrderService orderService;

    @Operation(summary = "REST API to create an Order")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiResponses(
            value = {
                    @ApiResponse( responseCode = "201",description = "Successfully created the Order")
            }
    )
    public Order saveOrder(@Valid @RequestBody Order order){
        return this.orderService.saveOrder(order);
    }

    @Operation(summary = "Rest API to expose orders")
    @GetMapping
    public Map<String, Object> fetchAll(@RequestParam(value = "pageno", defaultValue = "1") int pageNo,
                                        @RequestParam(value = "size", defaultValue = "10") int size,
                                        @RequestParam(value = "order", defaultValue = "orderId" ) String order){
        return this.orderService.fetchAll(pageNo, size, order);
    }

    @GetMapping("/{id}")
    @ApiResponses(
            value = {
                    @ApiResponse( responseCode = "200",description = "Returns the Order by its id"),
                    @ApiResponse(responseCode = "404", description = "If the Order with the id is not found")
            }
    )
    public Order fetchOrderById(@Parameter (name="orderId", required = true, description = "orderid to be fetched", example = "401") @PathVariable("id") long orderId){
        return this.orderService.findOrderById(orderId);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderById(@PathVariable("id") long orderId){
        this.orderService.deleteOrderById(orderId);
    }
}