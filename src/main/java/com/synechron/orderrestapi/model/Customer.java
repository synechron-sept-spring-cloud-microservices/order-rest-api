package com.synechron.orderrestapi.model;

import lombok.*;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="customers")
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@EqualsAndHashCode(of = {"id", "name", "emailAddress"})
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;

    private String emailAddres;

    private LocalDate dob;

 /*   @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Order> orders;*/

  /*  public void addOrder(Order order){
        if(this.orders == null){
            this.orders = new HashSet<>();
        }
        this.orders.add(order);
        //order.setCustomer(this);
    }*/
}