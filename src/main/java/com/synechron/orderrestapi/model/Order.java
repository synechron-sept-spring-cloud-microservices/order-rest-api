package com.synechron.orderrestapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Entity
@Table(name="orders")
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@EqualsAndHashCode(of = {"orderId", "price"})
//@ToString(exclude = "customer")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long orderId;

    @Min(value = 25000, message = "Min order price should be 25000")
    @Max(value = 50000 , message = "Max order price cannot be greater than 50000")
    private double price;

    @PastOrPresent(message = "Order date cannot be in the future")
    private LocalDate orderDate;

  /*  @ManyToOne
    @JoinColumn(name = "customer_id")
    @JsonIgnore
    private Customer customer;*/
}