package com.synechron.orderrestapi.config;

import com.github.javafaker.Faker;
import com.github.javafaker.Name;
import com.synechron.orderrestapi.model.Customer;
import com.synechron.orderrestapi.model.Order;
import com.synechron.orderrestapi.repository.CustomerRepository;
import com.synechron.orderrestapi.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static java.util.stream.IntStream.range;

@Configuration
@RequiredArgsConstructor
public class BootstapAppData implements ApplicationListener<ApplicationReadyEvent> {

    private final CustomerRepository customerRepository;
    private final OrderRepository orderRepository;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        Faker faker = new Faker();
        range(0,2000).forEach((index)-> {
            final Name customerName = faker.name();
            final Date birthday = faker.date().birthday(18, 45);
            Customer customer = Customer.builder()
                                            .name(customerName.fullName())
                                            .emailAddres(customerName.firstName()+ "@"+ faker.internet().domainName())
                                            .dob(birthday.toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                                        .build();
            final int orderRange = faker.number().numberBetween(2, 6);
            range(1, orderRange).forEach((i) -> {
                final Order order = Order.builder()
                        .orderDate(LocalDate.of(faker.number().numberBetween(1980, 2000), faker.number().numberBetween(1, 12), faker.number().numberBetween(1, 30)))
                        .price(faker.number().randomDouble(2, 25000, 45000))
                        .build();
             //   customer.addOrder(order);
                this.orderRepository.save(order);
            });

            this.customerRepository.save(customer);
            });
    }
}