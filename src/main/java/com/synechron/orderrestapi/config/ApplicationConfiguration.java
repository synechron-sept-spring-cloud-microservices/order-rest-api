package com.synechron.orderrestapi.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

@Configuration
@RequiredArgsConstructor
public class ApplicationConfiguration  { //implements CommandLineRunner {

    private final ApplicationContext applicationContext;
    private final Environment environment;


    public void run(String... args) throws Exception {
        System.out.println(" Inside the Application Configuration class :::");
        final String[] beanDefinitionNames = this.applicationContext.getBeanDefinitionNames();
        for(String beanName : beanDefinitionNames){
            if(beanName.contains("user")) {
                System.out.println("Bean Name :: " + beanName);
            }
        }
        System.out.println("Environment name :: "+ this.environment.getActiveProfiles().length);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}