package com.synechron.orderrestapi.config;

import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationService {

    @Bean
    @ConditionalOnProperty(prefix = "app", name = "loadUser", havingValue = "false")
    public User user(){
        return new User();
    }

    @Bean
    @ConditionalOnMissingBean(name = "user")
    public User userBasedOnMissingCondition(){
        return new User();
    }

    @Bean
    @ConditionalOnBean(name="user")
    public User userBasedOnCondition(){
        return new User();
    }
}

class User {

}

class Test {

}