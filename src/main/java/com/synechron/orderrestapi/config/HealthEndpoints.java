package com.synechron.orderrestapi.config;

import com.synechron.orderrestapi.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

@Component
class KafkaHealthEndpoint implements HealthIndicator {
    @Override
    public Health health() {
        //talk to the kafka topic and see the response
        return Health.status(Status.UP).withDetail("Kafka-Service", "Kafka service is up").build();
    }
}

@Component
@RequiredArgsConstructor
@Slf4j
class DBHealthEndpoint implements HealthIndicator{

    private final CustomerRepository customerRepository;

    @Override
    public Health health() {
        try {
            this.customerRepository.count();
        } catch (Exception exception){
            log.error("DB service is down :: {}", exception.getMessage());
            return Health.status(Status.DOWN).withDetail("DB-Service", exception.getMessage()).build();
        }
        return Health.status(Status.UP).withDetail("DB-Service", "DB service is up").build();
    }
}