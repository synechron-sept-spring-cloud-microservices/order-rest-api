package com.synechron.orderrestapi.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
@Slf4j
public class TrackingFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
      log.info("Inside the order service microservice {} :: " , ((HttpServletRequest)servletRequest).getHeader("CORRELATION_ID") );
      filterChain.doFilter(servletRequest, servletResponse);
    }
}