package com.synechron.orderrestapi.service;

import com.synechron.orderrestapi.model.Order;
import com.synechron.orderrestapi.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;

    private final Source source;

    //@CircuitBreaker(name = "inventoryservice", fallbackMethod = "fallBackImpl")
    @Retry(name = "inventoryservice", fallbackMethod = "fallBackImpl")
    public Order saveOrder(Order order){
        //call the inventory service and update the inventory count
        //1. approach - Using Discovery client
       // updateInventoryUsingDiscoveryClient(order);
        //2. approach - Using client side load balancer
        log.info("Calling the Inventory service :::::: " + System.currentTimeMillis());
       // updateInventoryUsingClientSideLoadBalancer();
        //3. Using FeignClient
       // this.inventoryFeignService.updateQuantity()
        //4. Using Kafka
        this.source.output().send(MessageBuilder.withPayload(order).build());
        return this.orderRepository.save(order);
    }

    private Order fallBackImpl(Throwable throwable){
        log.error("Exception while executing the REST API:: {} ", throwable.getMessage());
        return Order.builder().orderId(1111).orderDate(LocalDate.now()).price(25000).build();
    }

    public Map<String, Object> fetchAll(int pageNo, int size, String orderId){
        PageRequest pageRequest = PageRequest.of(pageNo, size, Sort.by(orderId));
        final Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);
        final long totalElements = pageResponse.getTotalElements();
        final int totalPages = pageResponse.getTotalPages();
        final List<Order> orders = pageResponse.getContent();
        Map<String, Object> response = new LinkedHashMap<>();
        response.put("total", totalElements);
        response.put("pages", totalPages);
        response.put("data", orders);
        return response;
    }

    @Transactional
    public Order findOrderById(long orderId){
        return this.orderRepository.findById(orderId).orElseThrow(() -> new IllegalArgumentException("Invalid Order Id"));
    }

    @Transactional
    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }
}