package com.synechron.orderrestapi.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient("INVENTORYSERVICE")
@Service
public interface InventoryFeignService {
    @PostMapping("/api/inventory")
    Long updateQuantity();
}